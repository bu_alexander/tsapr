package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.InputVerifier;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import net.miginfocom.swing.MigLayout;
import windings.TDiskWinding;
import windings.TWinding;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WindingInputWindow extends JFrame {

	final static int MIN_WINDOW_WIDTH = 1100;
	final static int MIN_WINDOW_HEIGTH = 600;

	private TDiskWinding winding;
	
	private JPanel paneWdgProp;
	private JPanel panelWdgCompare;
	private JTabbedPane tabbedPane;
	private JTextField txtWndName;
	private JTextField txtWndVoltage;
	private JTextField txtMinWndCurrent;
	private JTextField txtNomWndCurrent;
	private JTextField txtMaxWndCurrent;
	private JComboBox<String> cmbWndDir;
	private JComboBox<String> cmbWndLic;
	private JTable table;
	private JTextField txtWndInnerD;
	private JTextField txtWndAverageD;
	private JTextField txtWndOuterD;
	private JTextField txtWndBottom_pos;
	private JTextField txtWndInner_rails;
	private JTextField txtWndOuter_rails;
	private JTextField txtSpacerWidth;
	private JTextField txtSpacerShrinc;
	private JTextField txtPaperSrinc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindingInputWindow frame = new WindingInputWindow(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * �������� ����
	 */
	public WindingInputWindow(TDiskWinding winding) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.winding = winding;
		
		setTitle("��������� �������");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGTH);
		setMinimumSize(new Dimension(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGTH));

		createMainMenu();
		
		createWdgPropPane();
		createWdgComparePane();
		createWndAxialStr();
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(panelWdgCompare, GroupLayout.PREFERRED_SIZE, 337, GroupLayout.PREFERRED_SIZE)
						.addComponent(paneWdgProp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 721, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(paneWdgProp, GroupLayout.PREFERRED_SIZE, 423, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panelWdgCompare, GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)))
					.addContainerGap())
		);
		
		getContentPane().setLayout(groupLayout);
	}

	/**
	 �������� ������ ����� ���� � ��������� ������� �������� ������� � ��������� ������� 
	 */
	private void createWndAxialStr() {
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		JScrollPane wdgAxialStructure = new JScrollPane();
		tabbedPane.addTab("������ �������� �������", null, wdgAxialStructure, null);
		table = new JTable();
		wdgAxialStructure.setViewportView(table);
		table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(true);
		table.setModel(new DefaultTableModel(
		new Object[][] { { null, null, null, null, null, null, null, null, null, null, null, null, null }, },
		new String[] { "\u2116", "\u0427\u0438\u0441\u043B\u043E \u043E.\u0432.", "//",
				"\u0427\u0438\u0441\u043B\u043E \u0445.\u0432.", "//",
				"\u043F\u0440. \u043E\u0441\u043D. (\u0435.\u043A.)", "\u043F\u0440. \u0445.\u0432.",
				"\u0434\u043E\u043F. \u0438\u0437\u043E\u043B.", "\u0440\u0430\u0437\u0433\u043E\u043D",
				"\u043A\u0430\u043D\u0430\u043B", "\u043E\u0441. \u0440\u0430\u0437\u043C\u0435\u0440",
				"\u0440\u0430\u0434. \u0440\u0430\u0437\u043C\u0435\u0440",
				"\u041E\u0431\u043E\u0437\u043D\u0430\u0447." }) {
			Class[] columnTypes = new Class[] { Integer.class, Double.class, Integer.class, Double.class, Integer.class,
					String.class, String.class, String.class, Double.class, Double.class, Double.class, Double.class,
					String.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(35);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(25);
		table.getColumnModel().getColumn(4).setPreferredWidth(25);
		table.getColumnModel().getColumn(5).setPreferredWidth(85);
		
		JScrollPane wdgWires = new JScrollPane();
		tabbedPane.addTab("���������� ", null, wdgWires, null);
	}

	/**
	 �������� ������� ���� �� ���������� ��������� ������� � ���������
	 */
	private void createWdgComparePane() {
		panelWdgCompare = new JPanel();
		panelWdgCompare.setBorder(new TitledBorder(null, "\u0421\u0440\u0430\u0432\u043D\u0435\u043D\u0438\u0435", TitledBorder.LEADING, TitledBorder.TOP, null, null));
	}

	/**
	 �������� ��������� ���� ���� 
	 */
	private void createMainMenu() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("����");
		menuBar.add(menu);
		
		JMenuItem OpenItem = new JMenuItem("�������");
		OpenItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Open!");
			}
		});
		OpenItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		menu.add(OpenItem);
		
		JMenuItem SaveItem = new JMenuItem("���������");
		SaveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		menu.add(SaveItem);
		
		JSeparator SeparatorItem1 = new JSeparator();
		SeparatorItem1.setOrientation(SwingConstants.VERTICAL);
		menu.add(SeparatorItem1);
		
		JMenuItem ExitItem = new JMenuItem("�����");
		menu.add(ExitItem);
	}
	
	public void fillWdgPropPane(TDiskWinding winding) throws Exception {
		if (winding != null) {
			txtWndName.setText(winding.getName());
			txtWndVoltage.setText(Double.toString(winding.getVoltage()));
			txtWndBottom_pos.setText(Double.toString(winding.getBottom_pos()));
			txtWndInner_rails.setText(Integer.toString(winding.getInner_rails()));
			txtWndOuter_rails.setText(Integer.toString(winding.getOuter_rails()));
			txtWndInnerD.setText(Double.toString(winding.getInnerD()));
			txtWndAverageD.setText(Double.toString(winding.getInnerD() + winding.getRadial()));
			txtWndOuterD.setText(Double.toString(winding.getInnerD() + 2*winding.getRadial()));
			if (winding.getDir() == TWinding.RIGHT)
				cmbWndDir.setSelectedIndex(0);
			else
				cmbWndDir.setSelectedIndex(1);
			if (winding.isLic())
				cmbWndLic.setSelectedIndex(0);
			else
				cmbWndLic.setSelectedIndex(1);
		    // ��������
		}
	}
	
	/**
	 �������� ����� ���� � ��������� ����������� �������
	 */
	private void createWdgPropPane() {
		paneWdgProp = new JPanel();

		paneWdgProp.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "��������� �������",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		paneWdgProp.setLayout(new MigLayout("", "[][][]", "[][][][][][][][][][][][][][][][]"));

		JLabel lblWndName = new JLabel("��� �������");
		paneWdgProp.add(lblWndName, "cell 0 0,alignx left");

		txtWndName = new JTextField();
		paneWdgProp.add(txtWndName, "cell 2 0,alignx left");
		txtWndName.setColumns(10);

		JLabel lblWndVoltage = new JLabel("����������� ���������� �������, �");
		paneWdgProp.add(lblWndVoltage, "cell 0 1,alignx left");

		txtWndVoltage = new JTextField();
		paneWdgProp.add(txtWndVoltage, "cell 2 1,alignx left");
		txtWndVoltage.setColumns(10);
		setDoubleVerifier(txtWndVoltage);

		JLabel lblWndCurrent = new JLabel("���, ����������� �� �������, �");
		paneWdgProp.add(lblWndCurrent, "cell 0 2,alignx left");

		JLabel label = new JLabel("���.");
		paneWdgProp.add(label, "cell 1 2");

		JLabel label_4 = new JLabel("���.");
		paneWdgProp.add(label_4, "cell 1 3,alignx left");

		JLabel label_5 = new JLabel("����.");
		paneWdgProp.add(label_5, "cell 1 4,alignx left");

		JLabel lblMinimum = new JLabel("���.");
		paneWdgProp.add(lblMinimum, "cell 1 2,alignx left");

		txtMinWndCurrent = new JTextField();
		paneWdgProp.add(txtMinWndCurrent, "flowx,cell 2 2,alignx left");
		txtMinWndCurrent.setColumns(10);
		setDoubleVerifier(txtMinWndCurrent);

		txtNomWndCurrent = new JTextField();
		paneWdgProp.add(txtNomWndCurrent, "flowx,cell 2 3,alignx left");
		txtNomWndCurrent.setColumns(10);
		setDoubleVerifier(txtNomWndCurrent);

		txtMaxWndCurrent = new JTextField();
		paneWdgProp.add(txtMaxWndCurrent, "flowx,cell 2 4,alignx left");
		txtMaxWndCurrent.setColumns(10);
		setDoubleVerifier(txtMaxWndCurrent);

		JLabel lblWndDir = new JLabel("����������� �������");
		paneWdgProp.add(lblWndDir, "cell 0 5,alignx left");

		cmbWndDir = new JComboBox<String>();
		cmbWndDir.setMaximumRowCount(2);
		cmbWndDir.setModel(new DefaultComboBoxModel<>(new String[] { "������", "�����" }));
		paneWdgProp.add(cmbWndDir, "cell 2 5,alignx left");

		JLabel lblWndLic = new JLabel("���� � ��������");
		paneWdgProp.add(lblWndLic, "cell 0 6,alignx left");

		cmbWndLic = new JComboBox<>();
		cmbWndLic.setModel(new DefaultComboBoxModel<>(new String[] { "��", "���" }));
		paneWdgProp.add(cmbWndLic, "cell 2 6,alignx left");

		JLabel lblWndDia = new JLabel("��������, ��");
		paneWdgProp.add(lblWndDia, "cell 0 7,alignx left");

		txtWndInnerD = new JTextField();
		paneWdgProp.add(txtWndInnerD, "flowx,cell 2 7,alignx left");
		txtWndInnerD.setColumns(10);
		setDoubleVerifier(txtWndInnerD);

		paneWdgProp.add(lblMinimum, "cell 1 7");

		JLabel label_1 = new JLabel("��.");
		paneWdgProp.add(label_1, "cell 1 8");

		txtWndAverageD = new JTextField();
		txtWndAverageD.setEditable(false);
		txtWndAverageD.setColumns(10);
		paneWdgProp.add(txtWndAverageD, "flowx,cell 2 8,alignx left");

		JLabel label_2 = new JLabel("����.");
		paneWdgProp.add(label_2, "cell 1 9");

		txtWndOuterD = new JTextField();
		txtWndOuterD.setEditable(false);
		txtWndOuterD.setColumns(10);
		paneWdgProp.add(txtWndOuterD, "flowx,cell 2 9,alignx left");

		JLabel lblWndBotDist = new JLabel("���������� �� ������� ����, ��");
		paneWdgProp.add(lblWndBotDist, "cell 0 10,alignx left");

		txtWndBottom_pos = new JTextField();
		paneWdgProp.add(txtWndBottom_pos, "cell 2 10,alignx left");
		txtWndBottom_pos.setColumns(10);
		setDoubleVerifier(txtWndBottom_pos);

		JLabel lblWndInnerRailNum = new JLabel("����� ���������� ����");
		paneWdgProp.add(lblWndInnerRailNum, "cell 0 11,alignx left");

		txtWndInner_rails = new JTextField();
		txtWndInner_rails.setColumns(10);
		paneWdgProp.add(txtWndInner_rails, "cell 2 11,alignx left");
		setIntegerVerifier(txtWndInner_rails);

		JLabel lblWndOuterRailNum = new JLabel("����� �������� ����");
		paneWdgProp.add(lblWndOuterRailNum, "cell 0 12,alignx left");

		txtWndOuter_rails = new JTextField();
		txtWndOuter_rails.setColumns(10);
		paneWdgProp.add(txtWndOuter_rails, "cell 2 12,alignx left");
		setIntegerVerifier(txtWndOuter_rails);

		JLabel lblWndSpacerWidth = new JLabel("������ ���������, ��");
		paneWdgProp.add(lblWndSpacerWidth, "cell 0 13,alignx left");

		txtSpacerWidth = new JTextField();
		txtSpacerWidth.setColumns(10);
		paneWdgProp.add(txtSpacerWidth, "cell 2 13,alignx left");
		setIntegerVerifier(txtSpacerWidth);

		JLabel lblWndSpacerSrinc = new JLabel("����������� ������ ���������");
		paneWdgProp.add(lblWndSpacerSrinc, "cell 0 14,alignx left");

		txtSpacerShrinc = new JTextField();
		txtSpacerShrinc.setColumns(10);
		paneWdgProp.add(txtSpacerShrinc, "cell 2 14,alignx left");
		setDoubleVerifier(txtSpacerShrinc);

		JLabel lblWndPaperSrinc = new JLabel("����������� ������ ������");
		paneWdgProp.add(lblWndPaperSrinc, "cell 0 15,alignx left");

		txtPaperSrinc = new JTextField();
		txtPaperSrinc.setColumns(10);
		paneWdgProp.add(txtPaperSrinc, "cell 2 15,alignx left");
		setDoubleVerifier(txtPaperSrinc);
	}
	
	private void setIntegerVerifier(JTextField component) {
		component.setInputVerifier(new InputVerifier() {
			
			@Override
			public boolean verify(JComponent input) {
				String ver_text = component.getText();
				if (!ver_text.equals("")) {
					try {
						Integer.parseInt(ver_text);
					} 
					catch (NumberFormatException e) {
						return false;
					}
				}
				return true;
			}
		});
	}

	private void setDoubleVerifier(JTextField component) {
		component.setInputVerifier(new InputVerifier() {
			
			@Override
			public boolean verify(JComponent input) {
				String ver_text = component.getText();
				if (!ver_text.equals("")) {
					try {
						Double.parseDouble(ver_text);
					} 
					catch (NumberFormatException e) {
						return false;
					}
				}
				return true;
			}
		});
	}
	
}
