package files_reader;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellReference;
import windings.*;
import wires.IWire;
import wires.TPbpuWire;
import wires.TPbuWire;
import wires.TPtbuWire;

import java.util.HashMap;
import java.util.Map;

public class Disk_winding_reader {
	// ������ � ������� �������
    final private static String NAME = "O2"; // ��� �������
    final private static String VOLTAGE = "O3"; // ��� �������
    final private static String DIR = "O6"; // ����������� �������
    final private static String INNERD = "O9"; // ���������� �������, ��
	final private static String BOTTOM_POS = "O10"; // ���������� �� ������� ����
	final private static String INNER_RAILS = "O11"; // ���������� ���������� ����
	final private static String OUTER_RAILS = "O12"; // ���������� ���������� ����
	final private static String SPACER_WIDTH = "O13"; // ������ ���������, ��
	
	final private static String DISKS_TABLE_START = "C51"; // ������ � ������� ���������� ���������� � ��������
	final private static String WIRES_TABLE_START = "AN3"; // ������ � ������� ���������� ���������� � ��������
	final private static String SR_TABLE_START = "AN14"; // ������ � ������� ���������� ���������� � ��������� �������
    final private static String TSWIRES_TABLE_START = "BG3"; // ������ � ������� ���������� ���������� � ��������
	
	/**
	 * �����, �������� �������� ���������� ������� �� excel �����
	 * @param winding ���������� �������
	 * @param sheet �������� ����� excel � ��������� �������
	 */
	public static void ReadWinding(TDiskWinding winding, final Sheet sheet) {
	    // ������ ��� �������
		CellReference cr = new CellReference(NAME);
		String sValue = sheet.getRow(cr.getRow()).getCell(cr.getCol()).getStringCellValue();
		winding.setName(sValue);

	    // ������ ���������� �������
		cr = new CellReference(VOLTAGE);
		winding.setVoltage(sheet.getRow(cr.getRow()).getCell(cr.getCol()).getNumericCellValue());

		// ������ ����������� ������� �������
		cr = new CellReference(DIR);
		sValue = sheet.getRow(cr.getRow()).getCell(cr.getCol()).getStringCellValue();		
		if (sValue.equals("�����")||sValue.equals("�����"))
			winding.setDir(TWinding.LEFT);
		else
			winding.setDir(TWinding.RIGHT);

		// ������ ���������� ������� �������
		cr = new CellReference(INNERD);
		double dValue = sheet.getRow(cr.getRow()).getCell(cr.getCol()).getNumericCellValue();
		winding.setInnerD(dValue);

		// ������ ���������� �� ������� ����
		cr = new CellReference(BOTTOM_POS);
		dValue = sheet.getRow(cr.getRow()).getCell(cr.getCol()).getNumericCellValue();
		winding.setBottom_pos(dValue);

		// ������ ���������� ���������� ����
		cr = new CellReference(INNER_RAILS);
		dValue = sheet.getRow(cr.getRow()).getCell(cr.getCol()).getNumericCellValue();
		winding.setInner_rails((int) dValue);

		// ������ ���������� �������� ����
		cr = new CellReference(OUTER_RAILS);
		dValue = sheet.getRow(cr.getRow()).getCell(cr.getCol()).getNumericCellValue();
		winding.setOuter_rails((int) dValue);

		// ������ ���������� �� �������� �������� �������
        Map<String, IWire> wires = new HashMap<>();
        wiresReader(sheet, new CellReference(WIRES_TABLE_START), wires);

		// ������ ���������� � �������� �������� ������ �������
        Map<String, TPbuWire> wiresTS = new HashMap<>();
        tSwiresReader(sheet, new CellReference(TSWIRES_TABLE_START), wiresTS);

        // ������ ���������� � ��������� ������� �������
        Map<String, TStatic_shield> srings = new HashMap<>();
        sRingsReader(sheet, new CellReference(SR_TABLE_START), srings);

		// ������ ���������� � �������� �������
		cr = new CellReference(DISKS_TABLE_START);
		int startrow = cr.getRow();
		int col = cr.getCol();
		int wndelements = (int) sheet.getRow(startrow).getCell(col).getNumericCellValue();
		for (int i = wndelements; i>0; i--){
            int row = startrow + i - 1;
            if (wires.containsKey(sheet.getRow(row).getCell(col + 11).getStringCellValue())) {
                double turns = sheet.getRow(row).getCell(col + 1).getNumericCellValue();
                if (sheet.getRow(row).getCell(col + 4).getNumericCellValue() != 0)
                    turns += sheet.getRow(row).getCell(col + 2).getNumericCellValue() /
                                sheet.getRow(row).getCell(col + 4).getNumericCellValue();
                double parallels = sheet.getRow(row).getCell(col + 5).getNumericCellValue();
                double ts_turns = sheet.getRow(row).getCell(col + 6).getNumericCellValue();
                if (sheet.getRow(row).getCell(col + 9).getNumericCellValue() != 0)
                    ts_turns += sheet.getRow(row).getCell(col + 7).getNumericCellValue() /
                                   sheet.getRow(row).getCell(col + 9).getNumericCellValue();
                double ts_parallels = sheet.getRow(row).getCell(col + 10).getNumericCellValue();
                String disktype = sheet.getRow(row).getCell(col + 27).getStringCellValue();
                IWire wire = wires.get(sheet.getRow(row).getCell(col + 11).getStringCellValue());
                TPbuWire wireTS = wiresTS.get(sheet.getRow(row).getCell(col + 12).getStringCellValue());
                TDisk disk = new TDisk();
                disk.setuChannel(new TChannel());
                disk.setTurns(turns);
                disk.setPwires((int) parallels);
                disk.setTurnsTS(ts_turns);
                disk.setPwiresTS((int) ts_parallels);
                disk.setWire(wire);
                disk.setWireTS(wireTS);
                disk.setType(disktype);
                winding.addDisk(disk);
            }
        }
	}

    private static void wiresReader(Sheet sheet, CellReference cr, Map<String, IWire> wires) {
        int row = cr.getRow();
        short col = cr.getCol();
        while (!sheet.getRow(row).getCell(col+1).getStringCellValue().equals("")){
            int strip_number = (int) sheet.getRow(row).getCell(col+6).getNumericCellValue();
            double strip_width = sheet.getRow(row).getCell(col+2).getNumericCellValue();
            double strip_height = sheet.getRow(row).getCell(col+4).getNumericCellValue();
            double paper_thickness = sheet.getRow(row + 1).getCell(col+2).getNumericCellValue();
            double sigma02 = sheet.getRow(row).getCell(col+7).getNumericCellValue();
            switch (sheet.getRow(row).getCell(col+1).getStringCellValue()){
            case "����":   wires.put(sheet.getRow(row).getCell(col).getStringCellValue(),
                            new TPtbuWire(strip_number, strip_width, strip_height, paper_thickness, sigma02));
                           break;
            case "����":   wires.put(sheet.getRow(row).getCell(col).getStringCellValue(),
                            new TPbpuWire(strip_number, strip_width, strip_height, paper_thickness, 0.51, sigma02));
                           break;
            case "����-�": wires.put(sheet.getRow(row).getCell(col).getStringCellValue(),
                            new TPbpuWire(strip_number, strip_width, strip_height, paper_thickness, 0.34, sigma02));
                           break;
            case "���":    wires.put(sheet.getRow(row).getCell(col).getStringCellValue(),
                            new TPbuWire(strip_width, strip_height, paper_thickness));
                           break;
            default:       throw new IllegalArgumentException("������ ��� ������ ���� �������.");
            }
            row += 2;
        }
    }

    private static void tSwiresReader(Sheet sheet, CellReference cr, Map<String, TPbuWire> wires) {
        int row = cr.getRow();
        short col = cr.getCol();
        while (sheet.getRow(row).getCell(col+2).getNumericCellValue() != 0){
            double strip_width = sheet.getRow(row).getCell(col+2).getNumericCellValue();
            double strip_height = sheet.getRow(row).getCell(col+4).getNumericCellValue();
            double paper_thickness = sheet.getRow(row + 1).getCell(col+2).getNumericCellValue();
            wires.put(sheet.getRow(row).getCell(col).getStringCellValue(),
                    new TPbuWire(strip_width, strip_height, paper_thickness));
            row += 2;
        }
    }

    private static void sRingsReader(Sheet sheet, CellReference cr, Map<String,TStatic_shield> sr) {
        int row = cr.getRow();
        short col = cr.getCol();
        while (sheet.getRow(row).getCell(col+7).getNumericCellValue() != 0){
            sr.put(sheet.getRow(row).getCell(col).getStringCellValue(),
                   new TStatic_shield(
                   sheet.getRow(row).getCell(col+7).getNumericCellValue(),
                   sheet.getRow(row + 1).getCell(col+7).getNumericCellValue()
            ));
            row += 2;
        }
    }

}
