package wires;

import windings.TDisk;

/**
 * ����� ������������ ������������� ����� ���������� ������
 * @author ���������
 *
 */
public class TWire {
	
	public static final int CU = 1; // ����
	public static final int AL = 0; // ��������
	
	private double strip_width; // ������� ������ (������) ������������� ����������, ��
	private double strip_height; // ������� ������ (������) ������������� ����������, ��
	private double corner_radius; // ������ ���������� ����������, ��
	private int material; // �������� ����������
	private double density; // ��������� ��������� ����������, ��/�^3
	private double resistivity; // �������� ������������� ���������� ��� 20���, ��*�
	private double sigma02; // �������� ������ ��������� ���������� 0.2, ���
	
	@Override
	public boolean equals(Object obj){
		if (obj == this) return true;
		if (obj == null || obj.getClass() != this.getClass()) { 
            return false; 
		}
		TWire temp = (TWire) obj;
		return  this.strip_width == temp.strip_width &&
				this.strip_height == temp.strip_height &&
				this.sigma02 == temp.sigma02 &&
				this.material == temp.material &&
				this.corner_radius == temp.corner_radius;
	}
	
	public TWire(){
		this.strip_height = -1; // ������ -1 ��������, ��� �� �� ��� ���������
		this.strip_width = -1;
		this.corner_radius = -1;
		setMaterial(CU);
		this.sigma02 = -1;
	}
	
	public int getMaterial() {
		return material;
	}
	
	public double getDensity() {
		return density;
	}

	public double getResistivity() {
		return resistivity;
	}

	public void setMaterial(int material) {
		if ((material != CU)&&(material != AL)) throw new IllegalArgumentException("����������� �������� ����������");
		if (material == CU){
			this.density = 8900; // ��������� ���� �� ���� 434-78
			this.resistivity = 0.01724E-6; // �������� ������������� ���� ��� 20��� �� ���� 434-78
		}
		else if (material == AL){
			this.density = 2710; // ��������� �������� �� ������ �.�. ���������������� ������� ��������������� � ��������� "������" 1981
			this.resistivity = 0.0279E-6; // �������� ������������� �������� ��� 20��� �� ������ �.�. � ���� 3484.1-88 
		}
		this.material = material;
	}
	
	public double getStrip_width() {
		if (strip_width == -1) throw new IllegalArgumentException("�� ��������� ������� ������ (������) ������������� ����������");
		return strip_width;
	}
	
	public void setStrip_width(double strip_width) {
		if (strip_width>0)
			this.strip_width = strip_width;
		else
			throw new IllegalArgumentException("������� ������ (������) ������������� ���������� ������ ���� ������ 0");
	}
	
	public double getStrip_height() {
		if (strip_height == -1) throw new IllegalArgumentException("�� ��������� ������� ������ (������) ������������� ����������");
		return strip_height;
	}
	
	public void setStrip_height(double strip_height) {
		if (strip_height>0)
			this.strip_height = strip_height;
		else
			throw new IllegalArgumentException("������� ������ (������) ������������� ���������� ������ ���� ������ 0");
	}
	
	public double getCorner_radius() {
		return corner_radius;
	}
	
	public void setCorner_radius(double corner_radius) {
		if (corner_radius>0)
		this.corner_radius = corner_radius;
	}

	public double getSigma02() {
		return sigma02;
	}

	public void setSigma02(double sigma02) {
		if (sigma02>0)
		this.sigma02 = sigma02;
	}
	/**
	 * <p>������������� ������ ����������� ������ ������������� ���������
	 *    � ������������ � ���� 434-78 </p>
	 * @param strip_width ������� ������ (������) ������������� ����������, ��
	 */
	public void setGostCorner(double strip_width){
		setCorner_radius(1.00);
		if (strip_width <= 1.00) setCorner_radius(0.50*strip_width);
		if (strip_width > 1.00 && strip_width <= 1.60)  setCorner_radius(0.50);
		if (strip_width > 1.60 && strip_width <= 2.24)  setCorner_radius(0.65);
		if (strip_width > 2.24 && strip_width <= 3.55)  setCorner_radius(0.80);
	}
	/**
	 * <p> ���������� ������� ������������� ���������� </p>
	 * @return ������� ����������� ������� ������������� ����������, ��^2
	 */
	public double getCrossSection(){
		return this.getStrip_width()*this.getStrip_height()
				-Math.pow(this.getCorner_radius()*2, 2)+Math.PI*Math.pow(this.getCorner_radius(), 2);
	}
	/**
	 * <p> ���������� ����� ���������� �� ���� </p>
	 * @return ����� ���������� �� ����, ��
	 */
	public double getStripWeightPerMeter(){
		return this.getDensity()*this.getCrossSection()/Math.pow(1000, 2);
	}

}
