package wires;

public class TDiskFiller {
	
	public static final int PRESSBOARD_1MM = 1;
	public static final int PRESSBOARD_2MM = 2;
	public static final int PRESSBOARD_SECTOR = 3;
	
	private int fillerType; // ��� ������� 1-1 �� ������; 2-2 �� ������; 3- ������;
	private double fillerSize; // ������ �������
	
	public TDiskFiller(double FillerSize){
		super();
		this.setFillerSize(FillerSize);	
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj == this) return true;
		if (obj == null || obj.getClass() != this.getClass()) { 
            return false; 
		}
		TDiskFiller temp = (TDiskFiller) obj;
		return  this.fillerSize == temp.fillerSize &&
				this.fillerType == temp.fillerType;
	}
		
	public int getFillerType() {
		return fillerType;
	}
	public void setFillerType(int fillerType) {
		this.fillerType = fillerType;
	}
	public double getFillerSize() {
		return fillerSize;
	}
	public void setFillerSize(double fillerSize) {
		this.fillerSize = fillerSize;
	}
}
