package wires;
/**
 * ����� ������������ �������������� ������ ���� ����
 * @author ���������
 *
 */
public class TPbpuWire extends TWire implements IWire {
	
	private int strip_number; // ���������� ������������ ����������� � �������������� �������
	private double paper_thickness; // ����� ������� ������������ ������ �� 2 �������, ��
	private double strip_paper_thickness; // ������� �������� �������� ������������� ���������� �� 2 �������, ��

	public TPbpuWire(){
		super();
		this.strip_number = 2;
		this.paper_thickness = -1;
		this.strip_paper_thickness = -1;
	}
	/**
	 * <p>������� �������������� ������ ����</p>
	 * @param strip_number ���������� ������������ ����������� � �������������� �������
	 * @param strip_width ������� ������ (������) ������������� ����������, ��
	 * @param strip_height ������� ������ (������) ������������� ����������, ��
	 * @param paper_thickness ����� ������� ������������ ������ �� 2 �������, ��
	 * @param strip_paper_thickness ������� �������� �������� ������������� ���������� �� 2 �������, ��
	 * @param sigma02 �������� ������ ��������� ���������� 0.2, ���
	 */
	public TPbpuWire(int strip_number, 
				     double strip_width, 
				     double strip_height, 
				     double paper_thickness, 
				     double strip_paper_thickness,
				     double sigma02){
		super();
		super.setStrip_height(strip_height);
		super.setStrip_width(strip_width);
		setGostCorner(strip_width);
		this.setStrip_number(strip_number);
		this.setPaper_thickness(paper_thickness);
		this.setStrip_paper_thickness(strip_paper_thickness);
		this.setSigma02(sigma02);
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj == this) return true;
		if (obj == null || obj.getClass() != this.getClass()) { 
            return false; 
		}
		TPbpuWire temp = (TPbpuWire) obj;
		return  super.equals(obj) &&
				this.paper_thickness == temp.paper_thickness &&
				this.strip_paper_thickness == temp.strip_paper_thickness &&
				this.strip_number == temp.strip_number;
	}
	
	/** ���������� ���������� ������������ ����������� ��������������� ������� */
	public int getStrip_number() {
		return strip_number;
	}
	
	/** ������������� ���������� ������������ ����������� ��������������� ������� */
	public void setStrip_number(int strip_number) {
		if (strip_number>=2)
			this.strip_number = strip_number;
		else
			throw new IllegalArgumentException("���������� ������������ ����������� ��������������� ������� ������ ���� �� ����� 2�");
	}
	
	/** ���������� ������� ����� �������� �������� ������� �� 2 �������, �� */
	public double getPaper_thickness() {
		return paper_thickness;
	}
	
	/** ������������� ������� ����� �������� �������� ������� �� 2 �������, �� */	
	public void setPaper_thickness(double paper_thickness) {
		if (paper_thickness>=0)
			this.paper_thickness = paper_thickness;
		else
			throw new IllegalArgumentException("������� ����� �������� �������� ������� �� 2 ������� ������ ���� �� ������ 0");
	}
	
	/** ���������� ������� �������� �������� ������������� ���������� �� 2 �������, �� */
	public double getStrip_paper_thickness() {
		return strip_paper_thickness;
	}
	
	/** ������������� ������� �������� �������� ������������� ���������� �� 2 �������, �� */
	public void setStrip_paper_thickness(double strip_paper_thickness) {
		if (strip_paper_thickness>=0)
			this.strip_paper_thickness = strip_paper_thickness;
		else
			throw new IllegalArgumentException("������� �������� �������� ������������� ���������� �� 2 ������� ������ ���� �� ������ 0");
	}
	
	@Override
	public double getWire_width() {
		return this.getStrip_width()*this.getStrip_number()+
			   (this.getStrip_number()-1)*this.getStrip_paper_thickness()
			   +this.getPaper_thickness();
	}
	
	@Override
	public double getWire_widthTS() {
		return this.getWire_width();
	}

	@Override
	public double getWire_height() {
		return this.getStrip_height() + this.getPaper_thickness();
	}
	
	@Override
	public double getWire_height(double shrinkage) {
		if (shrinkage>=0 && shrinkage<=1)
			return this.getStrip_height() + this.getPaper_thickness()*shrinkage;
		else
			throw new IllegalArgumentException("������� �������� �������� ������ ��������� � �������� [0;1]");
	}	
	
	@Override
	public double getConductorWeightPerMeter(){
		return this.getStripWeightPerMeter()*this.getStrip_number();
	}
	
	@Override
	public double getWeightPerMeter(){
		final double gamma1 = 0.78; // �������� ��� ��������� ������������ ����������� � ��������� ����������� ������, �/��^3
		final double gamma2 = 1.10; // �������� ��� ���������������� �������������� ����������� ������, �/��^3
		final double k1 = 1.00; // ����������� ����������� ������ ���������������� ������
		final double k2 = 1.14; // ����������� ����������� ������ ��������� ������		
		double m2 = (((this.getStrip_width()+this.getStrip_paper_thickness())*(this.getStrip_height()+this.getStrip_paper_thickness())
				-super.getCrossSection())*this.getStrip_number()+(this.getPaper_thickness()-this.getStrip_paper_thickness()-0.24)
				*(this.getStrip_number()*(this.getStrip_width()+this.getStrip_paper_thickness())+this.getStrip_height()
				+this.getPaper_thickness()-0.24))*gamma2*k1+0.24*((this.getPaper_thickness()+this.getStrip_paper_thickness())
				+this.getStrip_number()*(this.getStrip_width()+this.getStrip_paper_thickness())+this.getStrip_height()
				+this.getPaper_thickness()-0.24)*gamma1*k2;
		return this.getConductorWeightPerMeter()+m2/1000;
	}
}
