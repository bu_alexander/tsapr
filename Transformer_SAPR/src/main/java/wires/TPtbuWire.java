package wires;

import windings.TDisk;

/**
 * ����� ������������ ����������������� ������ ���� ����
 * @author ���������
 *
 */
public class TPtbuWire extends TWire implements IWire{

	private int strip_number; // ���������� ������������ ����������� � ����������������� �������
	private double epoxy_thickness; // ������� �������� ���� �� 2 �������, ��
	private double enamel_thickness; // ������� ����� �� 2 �������, ��
	private double paper_thickness; // ������� ������������ ������ �� 2 �������, ��
	private double spacer_thickness; // �������� ������������ ��������� ����� ������ ��������, ��
	
	public TPtbuWire(){
		super();
		this.strip_number = -1; // ���� �� ������ ���������� ������������ ����������� -1 
		this.epoxy_thickness = 0.05; // �� ��������� 0.05 ��
		this.enamel_thickness = 0.1; // �� ��������� 0.1 ��
		this.paper_thickness = -1; // ���� �� ������ ������� ������ -1
		this.spacer_thickness = -1; // ���� �� ������ ������� ��������� -1
	}
	/**
	 *  <p> ������� ����������������� ������ ���� </p>
	 *  
	 *  <p> ������� �������� �������� 0,1 ��
	 *  ������� �������� ���� 0,05 �� <br>
	 *  ������ ����������� ������������ ����������� � ������������ � ���� 434-78 </p>
	 *  
	 * @param strip_number ���������� ������������ ����������� � ����������������� �������
	 * @param strip_width ������� ������ (������) ������������� ����������, ��
	 * @param strip_height ������� ������ (������) ������������� ����������, ��
	 * @param paper_thickness ������� ������������ ������ �� 2 �������, ��
	 * @param sigma02 �������� ������ ��������� ���������� 0.2, ���
	 */
	public TPtbuWire(int strip_number, double strip_width, double strip_height, double paper_thickness, double sigma02){
		super();
		this.setStrip_number(strip_number);
		this.setStrip_height(strip_height);
		this.setStrip_width(strip_width);
		this.setGostCorner(strip_width);
		this.setPaper_thickness(paper_thickness);
		this.setSigma02(sigma02);
		this.setEnamel_thickness(0.1);
		this.setEpoxy_thickness(0.05);
		this.setSpacer_thickness();
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj == this) return true;
		if (obj == null || obj.getClass() != this.getClass()) { 
            return false; 
		}
		TPtbuWire temp = (TPtbuWire) obj;
		return  super.equals(obj) &&
				this.enamel_thickness == temp.enamel_thickness &&
				this.epoxy_thickness == temp.epoxy_thickness &&
				this.spacer_thickness == temp.spacer_thickness &&
				this.paper_thickness == temp.paper_thickness &&
				this.strip_number == temp.strip_number;
	}
	
	/** ���������� ���������� ������������ ����������� ������������������ ������� */
	public int getStrip_number() {
		return strip_number;
	}
	
	/** ������������� ���������� ������������ ����������� ������������������ ������� */
	public void setStrip_number(int stripnumber) {
		if (stripnumber>0)
			this.strip_number = stripnumber;
		else
			this.strip_number = -1;
	}
	
	/** ���������� ������� �������� ���� �� 2 �������, �� */
	public double getEpoxy_thickness() {
		return epoxy_thickness;
	}
	
	/** ������������� ������� �������� ���� �� 2 �������, �� */
	public void setEpoxy_thickness(double epoxythickness) {
		if (epoxythickness>=0)
			this.epoxy_thickness = epoxythickness;
		else
			this.epoxy_thickness = -1;
	}

	/** ���������� ������� ����� �� 2 �������, �� */
	public double getEnamel_thickness() {
		return enamel_thickness;
	}
	
	/** ������������� ������� ����� �� 2 �������, �� */	
	public void setEnamel_thickness(double enamelthickness) {
		if (enamelthickness>0)
			this.enamel_thickness = enamelthickness;
		else
			this.enamel_thickness = -1;
	}
	
	/** ���������� ������� �������� �������� ������� �� 2 �������, �� */
	public double getPaper_thickness() {
		return paper_thickness;
	}
	
	/** ������������� ������� �������� �������� ������� �� 2 �������, �� */	
	public void setPaper_thickness(double paperthickness) {
		if (paperthickness>=0)
			this.paper_thickness = paperthickness;
		else
			this.paper_thickness = -1;
	}
	
	/** ���������� ������� ������������ ��������� ����� ������ ��������, �� */	
	public double getSpacer_thickness() {
		return spacer_thickness;
	}
	
	/**
	 * ������������� ������� ������������ ��������� ����� ������ ��������
	 * � ����������� � �� 3591-103-85636722-2010
	 */
	public void setSpacer_thickness() {
		double a = this.getStrip_width();
		double n = this.getStrip_number();
		double E = 0.15; // ����������� ��������� ������� �������� �������� � ������� �����
		if ((a+E)*(n-3)/2<8)
			this.setSpacer_thickness(0);
		else
			this.setSpacer_thickness(0.2);
	}
	
	/** ������������� ������� ������������ ��������� ����� ������ ��������, �� */
	public void setSpacer_thickness(double spacerthickness) {
		if (spacerthickness>=0)
			this.spacer_thickness = spacerthickness;
		else
			this.spacer_thickness = -1;
	}
	
	@Override
	public double getWire_width() {
	// � ������������ � TDE-2462-202
		double n = strip_number;
		double t = this.getStrip_width();
		double i = this.getPaper_thickness();
		double Z = 0;
		double k = 0;
		if (this.getSigma02()>90) 
			Z = 0.0004*Math.pow((34-n),2);
		else
			if (this.getStrip_width()<=1.5) 
				Z = 0;
			else
				Z = 0.00025*Math.pow((34-n), 2);
		if (this.getStrip_number()<=21)
			k = 1.030;
		else
			if (this.getStrip_number()<=25)
				k = 1.025;
			else
				k = 1.020;	
		double B = ((n+1)/2+Z)*(t+epoxy_thickness+enamel_thickness)*k+i;
		return B;
	}
	
	@Override
	public double getWire_widthTS() {
		int n = strip_number;
		double t = this.getStrip_width();
		double i = this.getPaper_thickness();
		double Z = 0.5;
		double k = 0;
		if (this.getStrip_number()<=21)
			k = 1.030;
		else
			if (this.getStrip_number()<=25)
				k = 1.025;
			else
				k = 1.020;	
		double B = ((n+1)/2+Z)*(t+epoxy_thickness+enamel_thickness)*k+i;
		return B;
	}

	@Override
	public double getWire_height() {
		return 2*(this.getStrip_height()+this.getEnamel_thickness()+this.getEpoxy_thickness())
				+this.getSpacer_thickness()+this.getPaper_thickness();
	}
	
	@Override
	public double getWire_height(double shrinkage) {
		return 2*(this.getStrip_height()+this.getEnamel_thickness()+this.getEpoxy_thickness())
				+(this.getSpacer_thickness()+this.getPaper_thickness())*shrinkage;		
	}
	
	@Override
	public double getConductorWeightPerMeter(){
		return this.getStripWeightPerMeter()*this.getStrip_number();
	}
	
	@Override
	public double getWeightPerMeter(){
		final double gamma2 = 1.26; // ��������� ����� ������ ��� �������������������� ����, �/��^3
		final double gamma3 = 1.15; // ��������� ����� ������ ����������� ����, �/��^3
		final double gamma4 = 1.00; // ��������� ������������������� ������ ����������� ���� �������� ��������, �/��^3
		final double gamma5 = 1.02; // ��������� ������������������� ������ �������� ���� �������� ��������, �/��^3;
		final double k1 = 1.00; // �����������, ����������� ������ ������ ����������� ���� �������� 
		final double k2 = 1.00; // �����������, ����������� ������ ������ �������� ���� ��������
		final double S = super.getCrossSection(); // ��������� ������� ������ ��������� ���� 434-78, ��^2;
		final double n = this.getStrip_number(); // ����� ������������ �����������
		final double a = this.getStrip_width(); // ����������� ������� ������ ������������� ���������, ��
		final double b = this.getStrip_height(); // ����������� ������ ������ ������������� ���������, ��
		final double r = super.getCorner_radius(); // ����������� ������ ����������� ��������� ������������� ����������, ��
		final double E1 =this.getEnamel_thickness(); // ����������� ��������� ������� ��������� ���� �������� �������� ��. ����������, ��
		final double E = E1 + this.getEpoxy_thickness();
		final double I = this.getSpacer_thickness();
		final double Z = this.getPaper_thickness(); // ����������� ��������� ������� �������� ��������, ��
		final int n1 = 2; // ���������� ���� �������� ���� �������� ��������
		final double delta1 =  0.105; // ����������� ������� ����� �������� ���� �������� ��������, ��
		// ��������� ������� ������� ����� ��������� ��������� ���� �������� ��������, ��^2
		final double S1 = (a+E1)*(b+E1)-0.86*Math.pow(r+E1/2, 2);
		//����� ����� �������� ������ ��������� ���� ������������ �����������, ��/��
		final double m2 = (S1-S)*gamma2*n;
		final double S2 = (a+E)*(b+E)-0.86*Math.pow(r+E/2, 2);
		final double m3 = (S2-S1)*gamma3*n;
		//����� �������������������� �������
		final double m4 = I*(a+E)*(n-3)/2;
		//����� ����������� ���� �������� �������� �������
		final double m5 = (Z/2-n1*delta1)*((a+E)*(n+1)+4*(b+E)+4*(Z/2-n1*delta1))*gamma4*k1;
		//����� �������� ���� �������� �������� �������
		final double m6 = n1*delta1*((a+E)*(n+1)+4*(b+E)+4*(Z-n1*delta1))*gamma5*k2;
		return this.getConductorWeightPerMeter() + (m2+m3+m4+m5+m6)/1000;
	}
}
