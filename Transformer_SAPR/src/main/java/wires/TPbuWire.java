package wires;
/**
 * ����� ������������ ������� ������ ���� ���
 * @author ���������
 *
 */
public class TPbuWire extends TWire implements IWire{
	
	private double paper_thickness; // ������� ������������ ������ �� 2 �������, ��
	
	public TPbuWire(){
		super();
		this.paper_thickness = -1;
	}
	
	/**
	 * <p>������� ������������� ������������� ������ ���</p>
	 * 
	 * <p>�������� ���������� - ����<br/>
	 *    ������ ����������� �� ���� 434-78<br/>
	 * </p>
	 * @param strip_width ������� ������ (������) ������������� ����������, ��
	 * @param strip_height ������� ������ (������) ������������� ����������, ��
	 * @param paper_thickness ������� ������������ ������ �� 2 �������, ��
	 */
	public TPbuWire(double strip_width, double strip_height, double paper_thickness){
		super();
		super.setStrip_height(strip_height);
		super.setStrip_width(strip_width);
		setGostCorner(strip_width);
		this.setPaper_thickness(paper_thickness);
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj == this) return true;
		if (obj == null || obj.getClass() != this.getClass()) { 
            return false; 
		}
		TPbuWire temp = (TPbuWire) obj;
		return  super.equals(obj) &&
				this.paper_thickness == temp.paper_thickness;
	}
	
	/** ���������� ������� �������� �������� ������� �� 2 �������, �� */
	public double getPaper_thickness() {
		return paper_thickness;
	}
	
	/** ������������� ������� �������� �������� ������� �� 2 �������, �� */
	public void setPaper_thickness(double paper_thickness) {
		if (paper_thickness>=0)
			this.paper_thickness = paper_thickness;
		else
			throw new IllegalArgumentException("������� ����� �������� �������� ������� �� 2 ������� ������ ���� �� ������ 0");
	}

	@Override
	public double getWire_height(){
		return this.getStrip_height() + this.paper_thickness;
	}
	
	@Override
	public double getWire_height(double shrinkage) {
		if (shrinkage>=0 && shrinkage<=1)
			return this.getStrip_height() + this.getPaper_thickness()*shrinkage;
		else
			throw new IllegalArgumentException("������� �������� �������� ������ ��������� � �������� [0;1]");
	}	
	
	@Override
	public double getWire_width(){
		return this.getStrip_width() + this.paper_thickness;
	}
	
	@Override
	public double getWire_widthTS() {
		return this.getWire_width();
	}

	@Override
	public double getConductorWeightPerMeter(){
		return this.getStripWeightPerMeter();
	}
	
	@Override
	public double getWeightPerMeter(){
		final double gamma4 = 1.10; // �������� ��� ���������������� �������������� ����������� ������, �/��^3
		final double gamma2 = 0.78; // �������� ��� ��������� ������������ ����������� � ��������� ����������� ������, �/��^3
		final double k = 1.17; // ����������� ����������� ������ ������
		double m7 = (this.getPaper_thickness()-0.24)*(this.getStrip_width()+this.getStrip_height()+this.getPaper_thickness()-0.24)
					*gamma4*k+0.24*(this.getStrip_width()+this.getStrip_height()+2*this.getPaper_thickness()-0.24)*gamma2*k;
		return this.getConductorWeightPerMeter()+m7/1000;
	}
	
}
