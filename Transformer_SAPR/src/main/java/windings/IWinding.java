package windings;

public interface IWinding {
	/**
	 * ���������� �������������� ������ �������
	 * @param pshrinkage ����������� ������ �������� ��������, �.�.
	 * @param pbshrinkage ����������� ������ ������������������� �������, �.�.
	 * @return �������������� ������ �������, ��
	 */
	double getGeom_height(double pshrinkage, double pbshrinkage);
	
	/**
	 * ���������� ������������� ������ �������
	 * @param pshrinkage ����������� ������ �������� ��������, �.�.
	 * @param pbshrinkage ����������� ������ ������������������� �������, �.�.
	 * @return ������������� ������ �������, ��
	 */
	double getEl_height(double pshrinkage, double pbshrinkage);
	
	/**
	 * ���������� ���������� ������ �������
	 * @return ���������� ������ �������, ��
	 */
	double getRadial();
	/**
	 * ���������� ��� ���� �������
	 * @return ��� ���� �������, ��
	 */
	double getCu_Weight();
	/**
	 * ���������� ��� �������
	 * @return ��� �������, ��
	 */
	abstract public double getWeight();
	/**
	 * ���������� ������������� ���� ������ �������
	 * @return ������������� ���� ������ �������, ��
	 */
	double getResistance();
	/**
	 * ���������� ����� ������ �������
	 * @return ����� ������������� ������ �������
	 */
	double getTurns();
}
