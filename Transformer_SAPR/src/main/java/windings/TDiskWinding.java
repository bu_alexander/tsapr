package windings;

import java.util.ArrayList;

public class TDiskWinding extends TWinding implements IWinding {

	// ������� (��������� ������) �������
	private ArrayList<IWindingElement> disks = new ArrayList<>();
	// ������ �������
	private ArrayList<TChannel> channels = new ArrayList<>();
	// ����������� �������� ���� �������
	private String topNode;
	// ����������� ������� ���� �������
	private String bottomNode;
	// ���� � ��������
	private boolean lic;
	
	/**
	 * ���������� ����������� �������� ����� �������
	 * @return ����������� �������� ����� �������
	 */
	public String getTopNode() {
		return topNode;
	}
	
	/**
	 * ������������� ����������� �������� ����� �������
	 * @param topNode ����������� �������� ����� �������
	 */
	public void setTopNode(String topNode) {
		this.topNode = topNode;
	}
	
	/**
	 * ���������� ����������� ������� ����� �������
	 * @return ����������� �������� ����� �������
	 */
	public String getBottomNode() {
		return bottomNode;
	}
	
	/**
	 * ������������� ����������� ������� ����� �������
	 * @param bottomNode ����������� ������� ����� �������
	 */
	public void setBottomNode(String bottomNode) {
		this.bottomNode = bottomNode;
	}
	/**
	 * ���������� ������� ����� � �������� �������
	 * @return ���� � �������� ������� (�� = true)
	 */
	public boolean isLic() {
		return lic;
	}
	/**
	 * ������������� ���� � �������� �������
	 * @param lic ���� � �������� ������� (�� = true)
	 */
	public void setLic(boolean lic) {
		this.lic = lic;
	}

	/**
	 * ��������� ������� ������� �������	
	 * @return ������ ������� �������
	 */
	public ArrayList<TChannel> getChannels(){
		return channels;
	}
	/**
	 * ��������� ������� ������� �������
	 * @return ������ ������� �������
	 */
	public ArrayList<IWindingElement> getDisks(){
		return disks;
	}
	
	/**
	 * <p>��������� ������� � ������� �������</p>
	 * <p>������ ������� ������� ����� ������ 0.</p>
	 * @param disk ����������� �������
	 */
	public void addDisk(TDisk disk){
		this.disks.add(disk);
	}
	
	/**
	 * <p>�������� ������� � �������� ��������</p>
	 * @param index ������ ������� � �������
	 * @param disk �������, �� ������� ������������ ������
	 */
	public void replaceDisk(int index, TDisk disk){
		disk.setDiskNumber(index+1);
		disk.setuChannel(this.disks.get(index).getuChannel());
		disk.setdChannel(this.disks.get(index).getdChannel());
		this.disks.set(index, disk);
	}
	
	/**
	 * <p>������� ������� � �������� ��������</p>
	 * <p>������ ������� ������� ����� ������ 0.</p>
	 * @param index ������ ������� � �������
	 */
	public void removeDisk(int index){
		this.disks.remove(index);
	}
	
	/**
	 * <p>��������� ����� � ������� �������</p>
	 * <p>����� ��� ������ �������� ��� ��������� ������� ����� ������ 0.</p>
	 * @param channel ����������� �����
	 */
	public void addChannel(TChannel channel){
		this.channels.add(channel);
	}	
	
	/**
	 * <p>�������� ����� � �������� ��������</p>
	 * @param index ������ ������ � �������
	 * @param channel �����, �� ������� ������������ ������
	 */
	public void replaceChannel(int index, TChannel channel){
		this.channels.set(index, channel);
	}
	
	/**
	 * <p>������� ����� � ��������� ��������</p>
	 * 
	 * @param index ������ ������ � �������
	 */
	public void removeChannel(int index){
		this.channels.remove(index);
	}
	
	/**
	 * <p>���������� ����� ��������� � ������� �������</p>
	 * @return ����� ��������� � ������� �������. ���� ����� ��������� � ������� ����������� ������������ -1
	 */
	public int getWindingSpacersNumber(){
		TChannel firstChannel = channels.get(0);
		boolean isIdentical = true;
		for (int i = 1; i < channels.size(); i++) {
			isIdentical &= (firstChannel.getSpacersNumber() == channels.get(i).getSpacersNumber());
		}
		return isIdentical ? firstChannel.getSpacersNumber() : -1;
	}
	
	@Override
	public double getTurns(){
		double turns = 0;
		for (IWindingElement item : disks)
			if (item instanceof TDisk) turns = turns + ((TDisk)item).getTurns();
			return turns;
	}

	@Override
	public double getGeom_height(double pshrinkage, double pbshrinkage) {
		double height = 0;
		// ���������� ��������� ������ �������
		for (IWindingElement item : disks) {
			height += item.getAxial(pshrinkage);
		}
		for (TChannel item : channels) {
			height = height + item.getHeight(pbshrinkage);
		}
		return Math.round(height);
	}

	@Override
	public double getEl_height(double pshrinkage, double pbshrinkage) {
		double El_height = this.getGeom_height(pshrinkage,pbshrinkage);
		// ������ ������� ��� ������� ��������� ����� � ������� ���/��� ����
		if (disks.get(0) instanceof TStatic_shield) {
			El_height = El_height - disks.get(0).getAxial(pshrinkage);
			El_height = El_height - channels.get(0).getHeight(pbshrinkage);
		}
		if (disks.get(disks.size()-1) instanceof TStatic_shield){
			El_height = El_height - disks.get(disks.size()-1).getAxial(pshrinkage);
			El_height = El_height - channels.get(channels.size()-1).getHeight(pbshrinkage);
		}
		return El_height;
	}

	@Override
	public double getRadial() {
		IWindingElement max_frq_disk = null; // �������� ����� ������������� �������
		int max_frq =1; // ����� �������� ����� ������������� �������
		for (int i=0; i<disks.size()-2; i++){
			int frq = 1;// ����� ������� ������� ����
			for (int k = i+1; k<disks.size()-1; k++)
				if (disks.get(i).equals(disks.get(k))) frq++;
			if (frq > max_frq){
				max_frq = frq;
				max_frq_disk = disks.get(i);
			}
		}
		if (max_frq_disk instanceof TStatic_shield)
			throw new IllegalStateException("��������� ������ �� ����� ���� ����� ����� ������������� ��������� �������");
		return ((TDisk)max_frq_disk).getRadial();
	}

	@Override
	public double getCu_Weight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getResistance() {
		// TODO Auto-generated method stub
		return 0;
	}

}