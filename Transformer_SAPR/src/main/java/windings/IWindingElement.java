package windings;

public interface IWindingElement {
	
	/**
	 * <p>������������� ��� �������� �������</p>
	 * @param type ��� �������� �������
	 */
	public void setType(String type);
	
	/**
	 * <p>���������� ������ ������ �������� �������</p>
	 * 
	 * @return ������ ������ �������� �������, ��
	 */
	public double getAxial();
	
	/**
	 * <p>���������� ������ ������ �������� �������</p>
	 * 
	 * @param shrinkage ����������� ������, �.�.
	 * @return ������ ������ �������� �������, ��
	 */
	public double getAxial(double shrinkage);

	/**
	 * <p>���������� ����� ��� ��������� �������</p>
	 * @return ����� ��� ��������� �������
	 */
	public TChannel getuChannel();
	
	/**
	 * <p>���������� ����� ��� ��������� �������</p>
	 * @return ����� ��� ��������� �������
	 */
	public TChannel getdChannel();
	
}
