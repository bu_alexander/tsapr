package windings;
/**
 * ����� ������������ ����� ��������� ��� ���� ����� �������
 */

public class TWinding {
	
	public static final byte LEFT = 0; // ����� ����������� �������
	public static final byte RIGHT = 1; // ������ ����������� �������
	
	private String name; // ��� �������
	private Double voltage; // ���������� ������������ �� ���� ������ ������� (��� ����� �������� ���������������)
	private byte dir; // ����������� ������� 0-����� 1-������
	private double innerD; // ��������� ������� �������, ��
	private double bottom_pos; // ���������� �� ���� ������� ���� �� ������� ����� �������, ��
	private int inner_rails; // ����� ���������� ����	
	private int outer_rails; // ����� �������� ����

	/**
	 * ���������� ��� �������
	 * @return ��� �������
	 */
	public String getName() {
		return name;
	}
	/**
	 * ������������� ��� �������
	 * @param name ��� �������
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * ���������� ������� ���������� �� ���� ������ �������
	 * @return ������� ���������� �� ���� ������ �������, �
	 */
	public double getVoltage() {
		return voltage;
	}
	/**
	 * ������������� ������� ���������� �� ���� ������ �������
	 * @param voltage ������� ���������� �� ���� ������ �������, �
	 */
	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}
	/**
	 * ���������� ����������� ������� �������
	 * @return ����������� ������� �������
	 */
	public byte getDir() {
		return dir;
	}
	/**
	 * ������������� ����������� ������� �������
	 * @param dir ����������� ������� �������
	 */
	public void setDir(byte dir) {
		if ((dir != LEFT)&&(dir != RIGHT)) throw new IllegalArgumentException("�������� ����������� �������");
		this.dir = dir;
	}
	/**
	 * ���������� ���������� ������� �������
	 * @return ���������� ������� �������, ��
	 */
	public double getInnerD() {
		return innerD;
	}
	/**
	 * ������������� ���������� ������� �������
	 * @return ���������� ������� �������, ��
	 */
	public void setInnerD(double innerD) {
		this.innerD = innerD;
	}
	/**
	 * ���������� ���������� �� ���� ������� ���� �� ������� ����� �������
	 * @return ���������� �� ���� ������� ���� �� ������� ����� �������, ��
	 */
	public double getBottom_pos() {
		return bottom_pos;
	}
	/**
	 * ������������� ���������� �� ���� ������� ���� �� ������� ����� �������
	 * @param bottom_pos ���������� �� ���� ������� ���� �� ������� ����� �������, ��
	 */
	public void setBottom_pos(double bottom_pos) {
		this.bottom_pos = bottom_pos;
	}
	/**
	 * ���������� ����� ���������� ����
	 * @return ����� ���������� ����
	 */
	public int getInner_rails() {
		return inner_rails;
	}
	/**
	 * ������������� ����� ���������� ����
	 * @param inner_rails ����� ���������� ����
	 */
	public void setInner_rails(int inner_rails) {
		this.inner_rails = inner_rails;
	}
	/**
	 * ���������� ����� �������� ����
	 * @return ����� �������� ����
	 */
	public int getOuter_rails() {
		return outer_rails;
	}
	/**
	 * ������������� ����� �������� ����
	 * @param outer_rails ����� �������� ����
	 */
	public void setOuter_rails(int outer_rails) {
		this.outer_rails = outer_rails;
	}
}