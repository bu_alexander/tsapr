package windings;

public class TStatic_shield implements IWindingElement {

	private String ss_type; // ��� ���������� ������
	private double core_height; // ������ ���� ���������� ������
	private double core_width; // ���������� ������ ���� ���������� ������
	private double paper_thickness; // ������� �������� �������� �� 2 �������
	/** ����� ������ ���������� ������ */
	private TChannel uChannel;
	/** ����� ����� ���������� ������ */
	private TChannel dChannel;
	
	public TStatic_shield() {
		// TODO Auto-generated constructor stub
	}

	public TStatic_shield(double core_height, double paper_thickness) {
		this.core_height = core_height;
		this.paper_thickness = paper_thickness;
	}

	public double getCore_height() {
		return core_height;
	}

	public void setCore_height(double core_height) {
		this.core_height = core_height;
	}

	public double getPaper_thickness() {
		return paper_thickness;
	}

	public double getPaper_thickness(double shrinkage) {
		return paper_thickness*shrinkage;
	}	

	public void setPaper_thickness(double paper_thickness) {
		this.paper_thickness = paper_thickness;
	}

	public double getCore_width() {
		return core_width;
	}

	public void setCore_width(double core_width) {
		this.core_width = core_width;
	}
	
	@Override
	public double getAxial() {
		return this.getCore_height() + this.getPaper_thickness();
	}
	
	@Override
	public double getAxial(double shrinkage) {
		return this.getCore_height() + this.getPaper_thickness(shrinkage);
	}
	
	@Override
	public TChannel getuChannel() {
		return uChannel;
	}

	public void setuChannel(TChannel uChannel) {
		this.uChannel = uChannel;
	}
	
	@Override
	public TChannel getdChannel() {
		return dChannel;
	}

	public void setdChannel(TChannel dChannel) {
		this.dChannel = dChannel;
	}

	@Override
	public void setType(String type) {
		this.ss_type = type;
	}
}
