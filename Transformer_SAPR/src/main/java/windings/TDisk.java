package windings;
/**
 * ����� ������������ ������� �������
 */
import wires.*;
import java.util.ArrayList;

public class TDisk implements IWindingElement {
	
	public static final int INNER_TRANSITION = 0; // ���������� �������
	public static final int OUTER_TRANSITION = 1; // �������� �������
	
	/** ����� ������� */
	private int DiskNumber;
	/**	��� ������� */
	private String DiskType;
	/** ������� ������ �������*/
	private int StartTransition;
	/** ������� ����� �������*/
	private int EndTransition;
	/** �������� ������ ������� */
	private IWire wire;
	/** ������ �������� ������ ������� */
	private TPbuWire wireTS;
	/** ����� ������������ �������� � ������� */
	private int pwires;
	/** ����� ������������ �������� �������� � ������� */
	private int pwiresTS;
	/** ����� ������ ������� */
	private double turns;
	/** ����� �������� ������ ������� */
	private double turnsTS;
	/** ����� ������ ������� */
	private TChannel uChannel;
	/** ����� ����� ������� */
	private TChannel dChannel;
	
	private ArrayList<Object> DiskElements= new ArrayList<Object>(); // ������ ��������� ������� (0 ������ - ���������� �������)

	public TDisk(){
		super();
	}
	
	/**
	  * <p>������� ������� �� ����� ���������</p>
	  * 
	  * @param wire �������� ������ �������
	  * @param wireTS ������ ��������� �����
	  * @param turns ����� ������ �������
	  * @param turnsTS ����� �������� ������ �������
	  */
	public TDisk(IWire wire, TPbuWire wireTS, double turns, double turnsTS) {
		super();
		setWire(wire);
		setWireTS(wireTS);
		setPwires(1);
		setPwiresTS(1);
		setTurns(turns);
		setTurnsTS(turnsTS);
		for (int i=0; i < (Math.ceil(turns)-Math.ceil(turnsTS)); i++) {
			addWire();
		}
		for(int i=0; i < Math.ceil(turnsTS); i++){
			addWireTS();
			addWire();
		}
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj == this) return true;
		if (obj == null || obj.getClass() != this.getClass()) { 
            return false; 
		}
		TDisk temp = (TDisk) obj;
		return this.DiskType.equals(temp.DiskType) &&
			   this.pwires == temp.pwires &&
			   this.pwiresTS == temp.pwiresTS &&
			   this.turns == temp.turns &&
			   this.turnsTS == temp.turnsTS &&
			   this.DiskElements.containsAll(temp.DiskElements);
	}
	
	/**
	 * ���������� ����� �������
	 * @return ����� �������
	 */
	public int getDiskNumber() {
		return DiskNumber;
	}
	/**
	 * ������������� ����� �������
	 * @param diskNumber ����� �������
	 */
	public void setDiskNumber(int diskNumber) {
		DiskNumber = diskNumber;
	}
	/**
	 * ���������� ��� �������
	 * @return ��� �������
	 */
	public String getDiskType() {
		return DiskType;
	}
	
	/**
	 * ������������� ��� �������
	 * @param type ��� �������
	 */
	@Override
	public void setType(String type) {
		DiskType = type;
	}
	/**
	 * ��������� ������ � ������� ��������� �������
	 * @param Filler ������ (�����������)
	 */
	public void addFiller(TDiskFiller Filler){
		DiskElements.add(Filler);
	}
	/**
	 * ��������� ������ � ��������� ��������� �������
	 * @param index ��������� � �������
	 * @param Filler ������ (�����������)
	 */
	public void addFiller(int index, TDiskFiller Filler){
		DiskElements.add(index, Filler);
	}
	/**
	 * ��������� ������ � ������� ��������� �������
	 */
	public void addWire() {
		DiskElements.add(wire);
	}
	/**
	 * ��������� ������ � ��������� ��������� �������
	 * @param index ��������� � �������
	 */
	public void addWire(int index) {
		DiskElements.add(index, wire);
	}
	/**
	 * ��������� ������ �������� ������ � ������� ��������� �������
	 */
	public void addWireTS() {
		DiskElements.add(wireTS);
	}
	/**
	 * ��������� ������ �������� ������ � ��������� ��������� �������
	 * @param index ��������� � �������
	 */
	public void addWireTS(int index) {
		DiskElements.add(index, wireTS);
	}
	/**
	 * ���������� �������� ������ �������
	 * @return �������� ������ �������
	 */
	public IWire getWire() {
		return wire;
	}
	/**
	 * ������������� �������� ������ �������
	 * @param wire �������� ������ �������
	 */
	public void setWire(IWire wire) {
		this.wire = wire;
	}
	/**
	 * ���������� ������ �������� ������ �������
	 * @return ������ �������� ������ �������
	 */
	public TWire getWireTS() {
		return wireTS;
	}
	/**
	 * ������������� ������ �������� ������ �������
	 * @param wireTS ������ �������� ������ �������
	 */
	public void setWireTS(TPbuWire wireTS) {
		this.wireTS = wireTS;
	}
	/**
	 * ���������� ����� ������ �������
	 * @return ����� ������ �������
	 */
	public double getTurns() {
		return turns;
	}
	/**
	 * ������������� ����� ������ �������
	 * @param turns ����� ������ �������
	 */
	public void setTurns(double turns) {
		if (turns < 0) throw new IllegalArgumentException("����� ������ ������� �� ����� ���� ������������� ������");
		else this.turns = turns;
	}
	/**
	 * ���������� ����� ������ �������
	 * @return ����� ������ �������
	 */
	public double getTurnsTS() {
		return turnsTS;
	}
	/**
	 * ������������� ����� �������� ������ �������
	 * @param turnsTS ����� �������� ������ �������
	 */
	public void setTurnsTS(double turnsTS) {
		if (turns < 0) throw new IllegalArgumentException("����� �������� ������ ������� �� ����� ���� ������������� ������");
		this.turnsTS = turnsTS;
	}
	/**
	 * ���������� ���������� ������ �������
	 * @return ���������� ������ �������, ��
	 * �������� �������� TDE - 2463-3
	 */
	public double getRadial(){
		double Radial = 0;
		if (wire instanceof TPtbuWire) {
			// ���������� ������ �������� � ������ ������������������ ������� 
			double N = Math.ceil(turns)*pwires;
			double N1 = Math.ceil(turnsTS)*pwiresTS;
			double b1 = wire.getWire_widthTS();
			double b2 = wire.getWire_width();
			double b3 =0;
			if (wireTS!=null) b3 = wireTS.getWire_width();
			Radial = N1*(b1+b3)+b2*(N-N1);
		}
		else {
			// ���������� ������ �������� ��������
			Radial = Math.ceil(turns)*pwires*wire.getWire_width();
			// ���������� ������ �������� ������
			if (wireTS!=null){
				Radial = Radial + Math.ceil(turnsTS)*pwiresTS*wireTS.getWire_width();
			}
		}
		// ���������� ������ ������� ��������
		for (Object Element: DiskElements) {
			if (Element instanceof TDiskFiller) Radial = Radial + ((TDiskFiller) Element).getFillerSize();
		}
		// ���� ����������� �������
		if (Radial<40)
			Radial = Radial*1.02;
		else
			Radial = Radial*1.01;
		// ����������
		if ((Radial-Math.floor(Radial))<0.2) return Math.floor(Radial);
		if (((Radial-Math.floor(Radial))>0.2)&&((Radial-Math.floor(Radial))<0.7)) return Math.floor(Radial)+0.5;
		return Math.ceil(Radial);
	}
	/**
	 * <p>���������� ������ ������ �������</p>
	 * ������ ������ ������� ����� ������� ������� ��������������� � ��� �������.
	 * @return ������ ������ �������, ��
	 */
	@Override
	public double getAxial(){
		return wire.getWire_height();
	}
	
	/**
	 * <p>���������� ������ ������ �������</p>
	 * ������ ������ ������� ����� ������� ������� ��������������� � ��� �������.
	 * @param shrinkage ����������� ������, �.�.
	 * @return ������ ������ �������, ��
	 */
	@Override
	public double getAxial(double shrinkage){
		return wire.getWire_height(shrinkage);
	}
	
	public int getPwires() {
		return pwires;
	}

	public void setPwires(int pwires) {
		this.pwires = pwires;
	}

	public int getPwiresTS() {
		return pwiresTS;
	}

	public void setPwiresTS(int pwiresTS) {
		this.pwiresTS = pwiresTS;
	}

	public int getStartTransition() {
		return StartTransition;
	}

	public void setStartTransition(int startTransition) {
		if ((startTransition!=INNER_TRANSITION)&&(startTransition!=OUTER_TRANSITION)) {
			throw new IllegalArgumentException("������ � ����������� ������ �������");
		}
		else {
			StartTransition = startTransition;
			if (StartTransition == INNER_TRANSITION)
				EndTransition = OUTER_TRANSITION;
			else
				EndTransition = INNER_TRANSITION;
		}
	}

	public int getEndTransition() {
		return EndTransition;
	}
	
	@Override
	public TChannel getuChannel() {
		return uChannel;
	}

	public void setuChannel(TChannel uChannel) {
		this.uChannel = uChannel;
	}
	
	@Override
	public TChannel getdChannel() {
		return dChannel;
	}

	public void setdChannel(TChannel dChannel) {
		this.dChannel = dChannel;
	}

	public int getSpacersNumber() {
		if (getuChannel() != null) 
			return getuChannel().getSpacersNumber();
		else
			return getdChannel().getSpacersNumber();
	}
	
	/**
	 * ��������� ������� �� ������� ������
	 * @return ������ ���������� ������
	 */
	public ArrayList<String> DiskCheck(){
		ArrayList<String> check_result = new ArrayList<String>();
		// �������� 
		check_result.add("������ 1 � ������� "+this.getDiskNumber()+" ���� "+this.getDiskType());
		check_result.add("������ 2 � ������� "+this.getDiskNumber()+" ���� "+this.getDiskType());
		// ��������
		return check_result;
	}

}
