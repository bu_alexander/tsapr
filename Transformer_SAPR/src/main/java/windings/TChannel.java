package windings;

public class TChannel {

	public static final int NO_COLLAR = 0;
	public static final int INNER_COLLAR = -1;
	public static final int OUTER_COLLAR = 1;
	public static final int NO_ADJ = 0;
	public static final int ADD_ADJ = 1;
	public static final int REMOVE_ADJ = -1;

	private class TCollar {
		public double upper_ch_height; // ������ ������ ��� ������������ ������������, ��
		public double collar_thickness; // ������� ������������ �����������, ��
		public double lower_ch_height; // ������ ������ ��� ����������� ������������, ��
		public int collarType; // ��� ����������� (INNER_COLLAR - ����������; OUTER_COLLAR - ��������)
	}
	
	private double height; // ������ ������, ��
	private int adjustable; // ������� ������������� ������ (REMOVE_ADJ - �� �������; ADD_ADJ - �� �������)
	private TCollar collar; // ��������� ������������ �����������, ��
	private int SpacersNumber; // ����� ���������(�����) � ������

	public TChannel(){
		super();
		this.height = -1;
		this.adjustable = NO_ADJ;
		this.collar = null;
	}
	
	/**
	 * <p>������� �������������� ����� ��� ������������ �����������</p>
	 * 
	 * @param spcNum ����� ���������(�����) � ������
	 * @param height ������ ������ ��� ����� ������, ��
	 */
	public TChannel(int spcNum, double height){
		super();
		this.setHeight(height);
		this.adjustable = NO_ADJ;
		this.collar = null;
		this.setSpacersNumber(spcNum);
	}
	
	/**
	 * <p>������� ������������ �����</p>
	 * 
	 * ������������ ����� �� ����� ����� ������������ �����������
	 * @param spcNum ����� ���������(�����) � ������
	 * @param height ������ ������ ��� ����� ������, ��
	 * @param adjustable ��� ������������� ������ (<code>REMOVE_ADJ</code> - �� �������; <code>ADD_ADJ</code> - �� �������)
	 */
	public TChannel(int spcNum, double height, int adjustable){
		super();
		this.setHeight(height);
		this.setAdjustable(adjustable);
		this.collar = null;
		this.setSpacersNumber(spcNum);
	}
	
	/**
	 * <p>������� ����� ��� ������� � ��� ������������ �����������</p>
	 * 
	 * @param spcNum ����� ���������(�����) � ������
	 * @param upper_ch_height ������ ������ ��� ������������ ������������, ��
	 * @param collar_thickness ������� ������������ �����������, ��
	 * @param lower_ch_height ������ ������ ��� ����������� ������������, ��
	 * @param collartype ��� ����������� (INNER_COLLAR - ����������; OUTER_COLLAR - ��������)
	 */	
	public TChannel(int spcNum,
					double upper_ch_height,
			  		double collar_thickness,
			  		double lower_ch_height,
			  		int collartype) {
		super();
		this.setHeight(upper_ch_height, collar_thickness, lower_ch_height, collartype);
		this.setAdjustable(NO_ADJ);
		this.setSpacersNumber(spcNum);
	}
			
	/**
	 * <p> ���������� ������ ������ ��� ����� ������ </p>
	 * @return ������ ������ ��� ����� ������, ��
	 */	
	public double getHeight() {
		if (height < 0) throw new IllegalStateException("������ ������ �� ���� ������");
		return height;
	}
	
	/**
	 * <p> ���������� ������ ������ � ������ ������ </p>
	 * @param pbshrinkage ����������� ������ ������������������� �������, �.�.
	 * @return ������ ������ � ������ ������, ��
	 */
	public double getHeight(double pbshrinkage) {
		if (height < 0) throw new IllegalStateException("������ ������ �� ���� ������");
		return height*pbshrinkage;
	}	
	
	/**
	 * <p> ��������� ������ ������ ��� ������������ ����������� </p>
	 * 
	 * @param height ������ ������ ��� ����� ������, ��
	 */
	public void setHeight(double height) {
		if (height < 0) throw new IllegalArgumentException("������ ������ �� ����� ���� ������������� ������");
		this.height = height;
	}
	
	/**
	 * <p>��������� ������ ������ ��� ������� � ��� ������������ �����������</p>
	 * 
	 * @param upper_ch_height ������ ������ ��� ������������ ������������, ��
	 * @param collar_thickness ������� ������������ �����������, ��
	 * @param lower_ch_height ������ ������ ��� ����������� ������������, ��
	 * @param collarType ��� ����������� (INNER_COLLAR - ����������; OUTER_COLLAR - ��������)
	 */
	public void setHeight(double upper_ch_height,
						  double collar_thickness,
						  double lower_ch_height,
						  int collarType) {
		this.collar = new TCollar();
		this.collar.upper_ch_height = upper_ch_height;
		this.collar.collar_thickness = collar_thickness;
		this.collar.lower_ch_height =lower_ch_height; 
		this.collar.collarType = collarType;
		this.height = collar.lower_ch_height + collar.collar_thickness + collar.upper_ch_height;
	}
	
	/**
	 * <p>���������� ��� ������������ �����������</p>
	 * 
	 * @return ��� ����������� (NO_COLLAR - ��� �����������; INNER_COLLAR - ����������; OUTER_COLLAR - ��������)
	 */
	public int getCollartype(){
		if (collar==null) 
			return NO_COLLAR;
		else
			return collar.collarType;
	}
	
	/**
	 * ���������� ��� ������������� ������
	 * @return ��� ������������� ������ (NO_ADJ - �� ������������; REMOVE_ADJ - �� �������; ADD_ADJ - �� �������)
	 */
	public int getAdjustable() {
		return adjustable;
	}
	
	/**
	 * ��������� ���� ������������� ������
	 * @param adjustable ��� ������������� ������ (<code>REMOVE_ADJ</code> - �� �������; <code>ADD_ADJ</code> - �� �������)
	 */
	public void setAdjustable(int adjustable) {
		this.adjustable = adjustable;
	}
	
	/**
	 * ���������� ����� ���������(�����) �������
	 * @return ����� ���������(�����) �������
	 */
	public int getSpacersNumber() {
		return SpacersNumber;
	}
	/**
	 * ������������� ����� ���������(�����) �������
	 * @param spacersNumber ����� ���������(�����) �������
	 */
	public void setSpacersNumber(int spacersNumber) {
		SpacersNumber = spacersNumber;
	}

}
