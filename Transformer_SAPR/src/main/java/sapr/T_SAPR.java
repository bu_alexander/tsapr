package sapr;

import windings.*;
import wires.*;
import report_builder.Winding_builder;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class T_SAPR {

	public static void main(String[] args) {
		
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet("Test");

		Winding_builder wind_builder = new Winding_builder(wb, sheet); 
		
		TDiskWinding winding = new TDiskWinding();
		TPtbuWire wire = new TPtbuWire(7, 1.80, 7.50, 1.36, 180);
		TPbuWire wireTS = new TPbuWire(1.80, 14.0, 2.0);
		
		for (int i = 0; i<65; i++){
			winding.addChannel(new TChannel(36, 3.2));
		}
		
		for (int i = 0; i<66; i++){
			TDisk diskJ = new TDisk(wire, null, 8.0+34.0/36.0, 0);
			if (i < 65) diskJ.setuChannel(winding.getChannels().get(i));
			if (i > 0) diskJ.setdChannel(winding.getChannels().get(i-1));
			diskJ.setDiskNumber(i+1);
			if (i%2!=0) 
				diskJ.setStartTransition(TDisk.INNER_TRANSITION);
			else
				diskJ.setStartTransition(TDisk.OUTER_TRANSITION);
			winding.addDisk(diskJ);
		}
		TDisk D = new TDisk(wire, wireTS, 7.0+34.0/36.0, 0+34.5/36.0);
		winding.replaceDisk(4, D);
		D = new TDisk(wire, wireTS, 7.0+34.0/36.0, 0+34.5/36.0);
		winding.replaceDisk(5, D);
		D = new TDisk(wire, wireTS, 7.0+34.0/36.0, 0+34.5/36.0);
		winding.replaceDisk(60, D);
		D = new TDisk(wire, wireTS, 7.0+34.0/36.0, 0+34.5/36.0);
		winding.replaceDisk(61, D);
		
		wind_builder.Build_disk_winding(winding, 1, 0);
		
		FileOutputStream fos = null;
		
		try {
			fos = new FileOutputStream("test.xls");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		try {
			wb.write(fos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Object disk: winding.getDisks()){
			System.out.println(((TDisk) disk).getRadial());
		}
		System.out.println(winding.getEl_height(1, 1));
		System.out.println(winding.getGeom_height(1, 1));
	}
}
