package report_builder;

import org.apache.poi.ss.usermodel.*;

import windings.TChannel;
import windings.TDisk;
import windings.TDiskWinding;

public class Winding_builder {
	public Workbook wb;
	public Sheet sheet;
	public int StartRow;
	public int StartCell;
	
	public Winding_builder(Workbook wb, Sheet sheet){
		this.wb = wb;
		this.sheet = sheet;
	}
	/**
	 * ��������� ���������� �������
	 * @param w ���������� �������
	 * @param row ����� ������ � ������� �������� ������� �������
	 * @param cell ����� ������� � ������� �������� ����� ������� �������
	 */
	public void Build_disk_winding(TDiskWinding w, int row, int cell){
		for (int i=w.getDisks().size(); i>=1; i--){
			if (w.getDisks().get(i-1) instanceof TDisk)
			Disk_build(row+2*(w.getDisks().size()-i), cell, (TDisk)w.getDisks().get(i-1),
					(i==w.getDisks().size())? null: w.getChannels().get(i-1));
		}
	}
	/**
	 * ��������� ������� ������� � ������ ��� ��� (����� ������� �������)
	 * 
	 * @param row ����� ������ � ������� �������� �������
	 * @param cell ����� ������� � ������� �������� ����� �������
	 * @param disk �������, ������� ������� ����������
	 */
	public void Disk_build(int row, int cell, TDisk disk, TChannel ch){
		Row r =sheet.createRow(row); // ������ �������
		Row rd = sheet.createRow(row+1); // ������ �������
		Row ru = sheet.createRow(row-1); // ������� �������
		// ������������ ������
		CellStyle stLeftCell = wb.createCellStyle();
		stLeftCell.setBorderBottom(BorderStyle.THIN);
		stLeftCell.setBorderTop(BorderStyle.THIN);
		stLeftCell.setBorderLeft(BorderStyle.THIN);
		stLeftCell.setBorderRight(BorderStyle.NONE);
		CellStyle stMiddleCell = wb.createCellStyle();
		stMiddleCell.setBorderBottom(BorderStyle.THIN);
		stMiddleCell.setBorderTop(BorderStyle.THIN);
		stMiddleCell.setBorderLeft(BorderStyle.NONE);
		stMiddleCell.setBorderRight(BorderStyle.NONE);
		CellStyle stRightCell = wb.createCellStyle();		
		stRightCell.setBorderBottom(BorderStyle.THIN);
		stRightCell.setBorderTop(BorderStyle.THIN);
		stRightCell.setBorderLeft(BorderStyle.NONE);
		stRightCell.setBorderRight(BorderStyle.THIN);
		CellStyle stInnertr = wb.createCellStyle();
		stInnertr.setBorderLeft(BorderStyle.THIN);
		CellStyle stOutertr = wb.createCellStyle();
		stOutertr.setBorderRight(BorderStyle.THIN);
		
		// ��������� ������� � ���������� �������
		// ��������� ������ �������
		Cell NumberCell = r.createCell(cell);
		NumberCell.setCellValue(disk.getDiskNumber());
		// ��������� ������ � ����� �������
		Cell C1 = r.createCell(cell+1);
		C1.setCellValue(disk.getDiskType());
		C1.setCellStyle(stLeftCell);
		// ��������� ������ � ������ �������� ������ �������
		Cell C2 = r.createCell(cell+2);
		int IntTurns = (int) Math.floor(disk.getTurns());
		double FracTurns = disk.getSpacersNumber()*disk.getTurns() - disk.getSpacersNumber()*IntTurns; 
		C2.setCellValue(String.valueOf(IntTurns)+" "+String.format("%(.1f", FracTurns)+"/"+String.valueOf(disk.getSpacersNumber())); 
		C2.setCellStyle(stMiddleCell);
		// ��������� ������ � ������ �������� ������ �������
		Cell C3 = r.createCell(cell+3);
		if (disk.getTurnsTS() != 0) {
			IntTurns = (int) Math.floor(disk.getTurnsTS());
			FracTurns = disk.getSpacersNumber()*disk.getTurnsTS() - disk.getSpacersNumber()*IntTurns; 
			C3.setCellValue(String.valueOf(IntTurns)+" "+String.format("%(.1f", FracTurns)+"/"+String.valueOf(disk.getSpacersNumber())); 			
		}
		C3.setCellStyle(stRightCell);
		// ���������� � ������ ��� ��������
		Cell ChannelCell = ru.createCell(cell+4);
		if (ch != null){
			ChannelCell.setCellValue(String.format("%(.1f", ch.getHeight())+
							((ch.getAdjustable() == TChannel.NO_ADJ) ? "" :
								(ch.getAdjustable() == TChannel.ADD_ADJ) ? " *" : " **")+
							((ch.getCollartype()==TChannel.NO_COLLAR) ? "" :
								(ch.getCollartype()==TChannel.INNER_COLLAR) ? " �" : " �")
							);
		}
		// ������ ��������
		if (disk.getStartTransition() == TDisk.INNER_TRANSITION) {
			// ������ �������
			Cell C = rd.createCell(cell+1);
			C.setCellStyle(stInnertr);
			// ������� �������
			C = ru.createCell(cell+3);
			C.setCellStyle(stOutertr);			
		}
		if (disk.getStartTransition() == TDisk.OUTER_TRANSITION) {
			// ������ �������
			Cell C = rd.createCell(cell+3);
			C.setCellStyle(stOutertr);
			// ������� �������
			C = ru.createCell(cell+1);
			C.setCellStyle(stInnertr);			
		}
	}
	
}
