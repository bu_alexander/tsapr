package windings;

import static org.junit.Assert.*;

import org.junit.Test;

import wires.TDiskFiller;
import wires.TPbuWire;
import wires.TPtbuWire;

public class TDiskTest {

	@Test
	public void testGetRadial() {
		// �������� �� ����.672748.001��1 ������� ��
		// ������� �
		TPtbuWire wire1 = new TPtbuWire(13, 1.50, 5.60, 0.96, 180);
		TPbuWire wireTS = new TPbuWire(1.80, 10.00, 2.00);
		double turns = 7+34.0/36.0;
		double turnsTS = 0;
		TDisk testdisk = new TDisk(wire1, wireTS, turns, turnsTS);
		assertEquals(106.5, testdisk.getRadial(), 0.01);
		// ������� �
		turns = 6+28.0/36.0;
		testdisk = new TDisk(wire1, wireTS, turns, turnsTS);
		testdisk.addFiller(3, new TDiskFiller(2.0*7));
		turnsTS = 0;
		assertEquals(107.0, testdisk.getRadial(), 0.01);
		// ������� �2
		turns = 6+34.0/36.0;
		turnsTS = 1+34.5/36.0;
		testdisk = new TDisk(wire1, wireTS, turns, turnsTS);
		testdisk.addFiller(3, new TDiskFiller(2.0*2));
		assertEquals(106.0, testdisk.getRadial(), 0.01);	
		// ������� �1
		turns = 6+34.0/36.0;
		turnsTS = 0+34.5/36.0;
		testdisk = new TDisk(wire1, wireTS, turns, turnsTS);
		testdisk.addFiller(3, new TDiskFiller(2.0*4));
		assertEquals(105.5, testdisk.getRadial(), 0.01);
		// ������� �
		turns = 6+34.0/36.0;
		turnsTS = 0;
		testdisk = new TDisk(wire1, wireTS, turns, turnsTS);
		testdisk.addFiller(3, new TDiskFiller(2.0*7));
		assertEquals(107.0, testdisk.getRadial(), 0.01);	
		// �������� �� ����.672648.002��1 ������� ��
		TPbuWire wire2 = new TPbuWire(2.24, 7.5, 1.35);
		wireTS = new TPbuWire(1.0, 6.7, 2.0);
		// ������� �1
		turns = 17+23.0/24.0;
		turnsTS = 2+22.5/24.0;
		testdisk = new TDisk(wire2, wireTS, turns, turnsTS);
		testdisk.addFiller(8, new TDiskFiller(2.0*1));
		assertEquals(76.5, testdisk.getRadial(), 0.01);
		// ������� �2
		turns = 18+23.0/24.0;
		turnsTS = 1+22.5/24.0;
		testdisk = new TDisk(wire2, wireTS, turns, turnsTS);
		testdisk.addFiller(8, new TDiskFiller(2.0*1));
		assertEquals(77.0, testdisk.getRadial(), 0.01);
		// ������� �
		turns = 19+8.0/24.0;
		turnsTS = 0;
		testdisk = new TDisk(wire2, wireTS, turns, turnsTS);
		testdisk.addFiller(8, new TDiskFiller(2.0*2));
		assertEquals(76.5, testdisk.getRadial(), 0.01);		
	}

}
