package windings;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import wires.TPbuWire;
import wires.TPtbuWire;
import windings.TDisk;

public class TDiskWindingTest {
	
	TDiskWinding winding = new TDiskWinding();
	
	@Before
	public void setUp() throws Exception {
		TPtbuWire wire = new TPtbuWire(7, 1.80, 7.50, 1.36, 180);
		TPbuWire wireTS = new TPbuWire(1.80, 14.0, 2.0);
		TDisk diskJ = new TDisk(wire, null, 8.0+34.0/36.0, 0);
		TDisk diskL1 = new TDisk(wire, wireTS, 7.0+34.0/36.0, 0+34.5/36.0);
		TChannel channel = new TChannel(36, 4.13532);
		for (int i = 0; i<66; i++){
			winding.addDisk(diskJ);
		}
		
		winding.replaceDisk(4, diskL1);
		winding.replaceDisk(5, diskL1);
		winding.replaceDisk(60, diskL1);
		winding.replaceDisk(61, diskL1);
		
		for (int i = 0; i<65; i++){
			winding.addChannel(channel);
		}
		
		winding.getDisks().forEach(item->item.setType("�"));
	}

	@Test
	public void testGetGeom_height() {
		assertEquals(1341.0, winding.getGeom_height(0.9, 0.93), 0.001);
	}
	
	@Test
	public void testGetEl_height() {
		assertEquals(1341.0, winding.getEl_height(0.9, 0.93), 0.001);
	}

	@Test
	public void testGetRadial() {
		assertEquals(91.0, winding.getRadial(), 0.0);
	}
	
	@Test
	public void testGetTurns() {
		assertEquals(586.3333, winding.getTurns(), 0.01);
	}
	
	@Test
	public void testGetSpacersNumres() {
		assertEquals(36, winding.getWindingSpacersNumber());
	}
	
}
