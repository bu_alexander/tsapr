package file_reader;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import files_reader.Disk_winding_reader;
import windings.TDiskWinding;
import windings.TWinding;

public class Disk_winding_reader_Test {
	
	private HSSFWorkbook wb;
	private HSSFSheet test_sheet;
	private TDiskWinding test_wnd;
	
	@Before
	public void setUp() throws Exception {
		InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream("input_file.xls"); 
		wb = new HSSFWorkbook(in);
		test_sheet = wb.getSheetAt(0);
		test_wnd = new TDiskWinding();
	}
	
	@Test
	public void testReadWinding() {
		Disk_winding_reader.ReadWinding(test_wnd, test_sheet);
		assertEquals("��",test_wnd.getName());
		assertEquals(TWinding.RIGHT, test_wnd.getDir()); 
		assertEquals(99.5, test_wnd.getBottom_pos(), 0.01);
		assertEquals(36, test_wnd.getInner_rails()); 
		assertEquals(36, test_wnd.getOuter_rails());
		assertEquals(91, test_wnd.getRadial(),0.01);
	}
	
	@After
	public void close_book() throws Exception {
		wb.close();
	}

}
