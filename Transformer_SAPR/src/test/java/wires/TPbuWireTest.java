package wires;

import static org.junit.Assert.*;

import org.junit.Test;

public class TPbuWireTest {
	
	@Test
	public void testgetConductorWeightPerMeter1(){
		TPbuWire wire = new TPbuWire(2.24, 7.50, 1.35);
		assertEquals(4864.0, wire.getConductorWeightPerMeter()*11094*3, 4864.0*0.0015);		
	}
	
	@Test
	public void testgetWeightPerMeter1() {
		TPbuWire wire = new TPbuWire(2.24, 7.50, 1.35);
		assertEquals(5473.6, wire.getWeightPerMeter()*11094*3, 1.0);
	}	
	
	@Test
	public void testgetConductorWeightPerMeter2(){
		TPbuWire wire = new TPbuWire(2.00, 9.00, 1.35);
		assertEquals(760.0, wire.getConductorWeightPerMeter()*1616.0*3, 760.0*0.0015);		
	}
	
	@Test
	public void testgetWeightPerMeter2() {
		TPbuWire wire = new TPbuWire(2.00, 9.00, 1.35);
		assertEquals(859.1, wire.getWeightPerMeter()*1616.0*3, 1.0);
	}	

	@Test
	public void testEqualsObject() {
		TPbuWire wire1 = new TPbuWire(2.00, 9.00, 1.35);
		TPbuWire wire2 = wire1;
		assertEquals(true, wire1.equals(wire2));
		TPbpuWire wire3 = new TPbpuWire();
		assertEquals(false, wire1.equals(wire3));
		TPbuWire wire4 = new TPbuWire(2.00, 9.00, 1.35);
		assertEquals(true, wire1.equals(wire4));
		TPbuWire wire5 = new TPbuWire(2.00, 10.00, 1.35);
		assertEquals(false, wire1.equals(wire5));
	}
	
}
