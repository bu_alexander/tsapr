package wires;

import static org.junit.Assert.*;

import org.junit.Test;

public class TPtbuWireTest {

	@Test
	public void testGetWire_height() {
		TPtbuWire wire = new TPtbuWire(7, 1.80, 7.50, 1.36, 180);
		assertEquals(16.66, wire.getWire_height(), 0.0001);
	}
	
	@Test
	public void testEqualsObject() {
		TPtbuWire wire1 = new TPtbuWire(7, 1.80, 7.50, 1.36, 180);
		TPtbuWire wire2 = wire1;
		assertEquals(true, wire1.equals(wire2));
		TPbpuWire wire3 = new TPbpuWire();
		assertEquals(false, wire1.equals(wire3));
		TPtbuWire wire4 = new TPtbuWire(7, 1.80, 7.50, 1.36, 180);
		assertEquals(true, wire1.equals(wire4));
		TPtbuWire wire5 = new TPtbuWire(7, 1.80, 8.50, 1.36, 180);
		assertEquals(false, wire1.equals(wire5));
	}

/*	@Test
	public void testGetConductorWeightPerMeter1() {
		TPtbuWire wire = new TPtbuWire(7, 1.80, 7.50, 1.36, 180);
		System.out.println(wire.getConductorWeightPerMeter()*2519*3);
		assertEquals(6186, wire.getConductorWeightPerMeter()*2519*3, 1.0);		
	}
	
	@Test
	public void testgetWeightPerMeter1() {
		TPtbuWire wire = new TPtbuWire(7, 1.80, 7.50, 1.36, 180);
		System.out.println(wire.getWeightPerMeter()*2519*3);
		assertEquals(6503.0, wire.getWeightPerMeter()*2519*3, 0);		
	}	
	
	@Test
	public void testGetConductorWeightPerMeter2() {
		TPtbuWire wire = new TPtbuWire(13, 1.50, 5.60, 0.96, 180);
		System.out.println(wire.getConductorWeightPerMeter()*2112.0*3);
		assertEquals(5999.0, wire.getConductorWeightPerMeter()*2112.0*3, 1.0);		
	}
	
	@Test
	public void testgetWeightPerMeter2() {
		TPtbuWire wire = new TPtbuWire(13, 1.50, 5.60, 0.96, 180);
		System.out.println(wire.getWeightPerMeter()*2112.0*3);
		assertEquals(6219.0, wire.getWeightPerMeter()*2112.0*3, 0);		
	}	
*/
}
