package wires;

import static org.junit.Assert.*;

import org.junit.Test;

public class TPbpuWireTest {

	@Test
	public void testEqualsObject() {
		TPbpuWire wire1 = new TPbpuWire(3, 1.5, 6.0, 1.35, 0.36, 180);
		TPbpuWire wire2 = wire1;
		assertEquals(true, wire1.equals(wire2));
		TPbpuWire wire3 = new TPbpuWire();
		assertEquals(false, wire1.equals(wire3));
		TPbpuWire wire4 = new TPbpuWire(3, 1.5, 6.0, 1.35, 0.36, 180);
		assertEquals(true, wire1.equals(wire4));
		TPbpuWire wire5 = new TPbpuWire(2, 1.5, 6.0, 1.35, 0.36, 180);
		assertEquals(false, wire1.equals(wire5));
	}

}
